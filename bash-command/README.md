### Bash для ubuntu 16.04:
Для початку дайте права на папки з командами
```
sudo chmod -R 777 ./{folder}
```
**Приклад запуску команди:**
```
make {command}
```
### Перечень команд
1. composer 	      - Установка компосера
2. docker   	      - Установка Docker і Docker-compose
3. git 	    	      - Установка git
4. gulp     	      - Установка gulp
5. lamp     	      - Установка apache2 + mySql + php7.0 + phpMyAdmin
6. php-storm-install  - Установка PhpStorm2018.3
7. php-strom-evaluate - Оновлення тріалки для PhpStorm2018.3
